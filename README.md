# AVALIAÇÃO PARCIAL - GESTÃO DE STARTUPS I 

Introdução ao tema do projeto e apresentação das técnicas que serão abordadas.

Assuntos abordados durante a disciplina: 
- Matriz de Necessidades
- Árvore do Problema
- Duplo Diamante
- Análise Pestel
- Análise SWOT
- Matriz CSD

Explicação da técnica SWOT e aplicação em relação ao projeto. Os alunos deverão fazer uma análise SWOT do projeto, identificando suas forças, fraquezas, oportunidades e ameaças. Os alunos deverão apresentar as suas análises e discutir as possibilidades de melhoria e potencialização do projeto.

Explicação da técnica Árvore do Problema e sua aplicação em relação ao projeto. Os alunos deverão construir uma árvore do problema para identificar as principais causas e consequências dos problemas relacionados ao projeto. Os alunos deverão apresentar suas árvores do problema e discutir as possibilidades de solução para os problemas identificados.

Explicação da técnica Análise PESTEL e sua aplicação em relação ao projeto. Os alunos deverão fazer uma análise PESTEL do projeto, identificando as principais variáveis políticas, econômicas, sociais, tecnológicas, ambientais e legais que possam influenciar o projeto. 

Explicação da técnica Duplo Diamante e sua aplicação em relação ao projeto. Os alunos deverão trabalhar em grupos para identificar as principais necessidades e desejos dos usuários relacionados ao projeto, utilizando a técnica do duplo diamante. Os alunos deverão apresentar as suas análises e discutir as possibilidades de melhorias e soluções para o projeto.

Explicação da técnica da Matriz de Necessidades e suas principais características. Discussão sobre a importância da técnica para o desenvolvimento de produtos e serviços mais adequados às necessidades dos usuários.

Explicação da técnica Matriz CSD e sua aplicação em relação ao projeto. Os alunos deverão utilizar a matriz CSD para priorizar as soluções identificadas anteriormente, considerando os critérios de certezas, suposições e dúvidas sobre o projeto.

Apresentação dos resultados finais do projeto e avaliação dos alunos sobre o curso.

- ATENÇÃO! 
É importante lembrar que o cronograma e roteiro podem ser ajustados de acordo com as necessidades e particularidades do projeto em questão. É fundamental que possam aplicar as técnicas de forma prática e que possam discutir suas análises e soluções em grupo, para um aprendizado de forma mais eficiente e compreender a aplicação das técnicas em situações reais.

Utilização de Post Its, Trello, Mural, Google Sheets e Slides para a apresentação. (Decorações, Banners e Cartazes serão permitidos).

- 4 EQUIPES - 7 PESSOAS POR EQUIPE 
- 1 PESSOA PARA GERENCIAR A EQUIPE (APRESENTAÇÃO, INTRODUÇÃO, CONCLUSÃO, ETC.) E AS DEMAIS 6 PESSOAS PARA EXPLANAR TODAS AS TÉCNICAS. 
- 5 - 7 MINUTOS PARA ORGANIZAÇÃO.
- 13 - 15 MINUTOS PARA APRESENTAÇÃO. 
- +/- 5 MINUTOS DE TOLERÂNCIA. 




